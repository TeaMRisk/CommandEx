package net.teamrisk;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class CommandEx extends JavaPlugin {

    public static CommandEx instance;
    public static HashMap<String, String> cmdList = new HashMap<String, String>();

    @Override
    public void onEnable() {
        saveDefaultConfig();
        instance = this;
        getCommand("commandex").setExecutor(new Commander());
        getServer().getPluginManager().registerEvents(new Listeners(), this);
        loadCommands();
    }

    public static void loadCommands() {
        cmdList.clear();
        for (String command : instance.getConfig().getKeys(false)) {
            cmdList.put(command, instance.getConfig().get(command.substring(0) + ".cmd").toString());
        }
    }
}
