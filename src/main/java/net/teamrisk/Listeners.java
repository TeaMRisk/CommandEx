package net.teamrisk;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.HashMap;

public class Listeners implements Listener {


    @EventHandler
    public void onCommandPreProcess(PlayerCommandPreprocessEvent e) {
        String msg = e.getMessage();
        String command = msg.substring(1);
        String cmd = command.split(" ")[0];
        if (CommandEx.cmdList.containsKey(cmd)) {
            String alias = CommandEx.instance.getConfig().getString(cmd + ".cmd");
            for (String change : alias.split(",")) {
                if (change != null) {
                    if (e.getPlayer().hasPermission("commandex." + cmd) || e.getPlayer().isOp()) {
                        if (CommandEx.instance.getConfig().getBoolean(cmd + ".servercmd")) {
                            e.getPlayer().getServer().dispatchCommand(Bukkit.getConsoleSender(), change + msg.substring(cmd.length() + 1));
                        } else {
                            e.getPlayer().chat("/" + change + msg.substring(cmd.length() + 1));
                        }
                        e.setCancelled(true);
                    } else {
                        return;
                    }
                }
            }
        }
    }
}