package net.teamrisk;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Map;

public class Commander implements CommandExecutor {

    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        if (args.length > 0) {
            if ((args[0].equals("create")) || (args[0].equals("delete")) || (args[0].equals("list")) || (args[0].equals("reload"))) {
                if (!sender.hasPermission("alias." + args[0])) {
                    sender.sendMessage(ChatColor.RED + "You don't have permission for this subcommand!");
                    return true;
                }
                if (args[0].equals("create")) {
                    if (args.length > 2) {
                        if (CommandEx.cmdList.get(args[1]) == null) {
                            StringBuilder sb = new StringBuilder();
                            for (int i = 2; i < args.length; i++) {
                                sb.append(args[i] + " ");
                            }
                            CommandEx.instance.getConfig().set(args[1] + ".cmd", sb.toString().trim());
                            CommandEx.instance.getConfig().set(args[1] + ".servercmd", false);
                            CommandEx.instance.saveConfig();
                            CommandEx.loadCommands();
                            sender.sendMessage(ChatColor.GRAY + "Created");
                        } else {
                            sender.sendMessage(ChatColor.RED + "Alias already exists with that name!");
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "Usage: /" + label + " create <aliasName> <commandName>");
                        sender.sendMessage(ChatColor.RED + "aliasName = The new command name, commandName = The old command");
                        sender.sendMessage(ChatColor.RED + "Ex: /" + label + " create gm gamemode");
                        sender.sendMessage(ChatColor.RED + "Would link /gm to /gamemode");
                    }
                } else if (args[0].equals("delete")) {
                    if (args.length > 1) {
                        if (CommandEx.cmdList.get(args[1]) == null) {
                            sender.sendMessage(ChatColor.RED + "Alias not found");
                        } else {
                            CommandEx.instance.getConfig().set(args[1], null);
                            CommandEx.instance.saveConfig();
                            CommandEx.loadCommands();
                            sender.sendMessage(ChatColor.GRAY + "Deleted");
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "Usage: /" + label + " delete <aliasName>");
                        sender.sendMessage(ChatColor.RED + "Removes the alias linked to aliasName");
                    }
                } else if (args[0].equals("list")) {
                    sender.sendMessage(ChatColor.GRAY + "[-" + ChatColor.GOLD + "CommandEx Alias List" + ChatColor.GRAY + "-]");
                    for (String key : CommandEx.cmdList.keySet()) {
                        String value = CommandEx.cmdList.get(key).toString();
                        sender.sendMessage(ChatColor.GRAY + " - /" + key + " => /" + value.replace(",", " /"));
                    }
                } else if (args[0].equals("reload")) {
                    CommandEx.instance.reloadConfig();
                    CommandEx.loadCommands();
                    sender.sendMessage(ChatColor.RED + "Config reloaded!");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Subcommand not found!");
            }
        } else {
            sender.sendMessage(ChatColor.GRAY + "[-" + ChatColor.GOLD + "CommandEx" + ChatColor.GRAY + "-]");
            formatCommandInfo(sender, label, "create", " <aliasName> <commandName>");
            formatCommandInfo(sender, label, "delete", " <aliasName>");
            formatCommandInfo(sender, label, "list", "");
            formatCommandInfo(sender, label, "reload", "");
        }
        return true;
    }

    private void formatCommandInfo(CommandSender sender, String label, String subcommand, String args) {
        sender.sendMessage(ChatColor.GRAY + " - /" + label + " " + subcommand + args + "  |  Permission: " + (sender.hasPermission("alias." + subcommand) ?
                ChatColor.GREEN + "Yes" : new StringBuilder().append(ChatColor.RED).append("No").toString()));
    }

}
